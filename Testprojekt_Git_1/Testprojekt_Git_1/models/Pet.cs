﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testprojekt_Git_1.models
{
    class Pet
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Pet():this(0, "", 0) { }
        public Pet(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
        public Pet(int id, string name, int age)
        {
            this.ID = id;
            this.Name = name;
            this.Age = age;
        }

        public override string ToString()
        {
            return "Alter: " + this.Age + "Name: " + this.Name + " ID: " + this.ID;
        }
    }
}
