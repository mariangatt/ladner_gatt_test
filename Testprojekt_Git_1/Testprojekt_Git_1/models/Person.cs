﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testprojekt_Git_1.models
{
    class Person
    {
        int ID { get; set; }
        string Firstname { get; set; }
        string Lastname { get; set; }
        int Age { get; set; }
        public Person() : this(0, "Vorname", "Nachname", 0) { }
        public Person(int id, string firstname, string lastname, int age)
        {
            this.ID = id;
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Age = age;
        }


        //Methoden
        public override string ToString()
        {
            return "ID: " + this.ID + " Vorname: " + this.Firstname + " Nachname: " + this.Lastname + " Alter: " + this.Age;
        }
    }
}
