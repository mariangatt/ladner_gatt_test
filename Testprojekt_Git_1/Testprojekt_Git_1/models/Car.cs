﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testprojekt_Git_1.models
{
    class Car
    {
        public string Model { get; set; }
        public int OwnerID { get; set; }
        public string Color { get; set; }
        public decimal Price { get; set; }

        public Car(): this(0,"","", 0.0m) { }
        public Car(int ownerid, string modell, string color, decimal price)
        {
            this.OwnerID = ownerid;
            this.Model = modell;
            this.Color = color;
            this.Price = price;

        }
        public override string ToString()
        {
            return "Besitzer - Identifikationsnummer:" + this.OwnerID + "Model: " + this.Model + " Farbe: "+ this.Color + " Preis: " + this.Price + "€"; 
        }
    }
}
